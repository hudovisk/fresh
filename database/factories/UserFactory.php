<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
        'fcm_token' => $faker->uuid,
        'uuid' => $faker->uuid,
    ];
});

$factory->defineAs(App\Models\User::class, 'data-register', function ($faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'password' => $password ?: $password = 'secrect',
        'password_confirmation' => $password,
        'fcm_token' => $faker->uuid,
    ];
});