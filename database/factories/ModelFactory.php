<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Advertisement::class, function (Faker\Generator $faker) {
    return [
        'uuid' => $faker->uuid,
        'title' => $faker->title,
        'description' => $faker->text,
        'tags' => str_replace(' ', ',', $faker->words(3, true)),
        'price' => $faker->numberBetween(10,100),
        'price_unit' => $faker->randomElement(['USD', 'BRL']),
        'published_at' => $faker->boolean() ? \Carbon\Carbon::now()->toDateTimeString() : null
    ];
});