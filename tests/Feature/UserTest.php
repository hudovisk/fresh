<?php

namespace Tests\Feature;

use App\Models\User;
use Laravel\Passport\Passport;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserTest extends TestCase
{

    use DatabaseTransactions;

    /**
     * Test user creation
     *
     * @return void
     */
    public function testCanRegisterUser()
    {
        $admin = factory(User::class)->create();
        $userDataToRegister = factory(User::class, 'data-register')->raw();

        $userDataToRegister['password'] = 'secrect';
        $userDataToRegister['password_confirmation'] = 'secrect';

        Passport::actingAs($admin);

        $response = $this->post('api/register', $userDataToRegister);

        $response->assertStatus(201);
        $response->assertJsonStructure([
            'message',
            'status'
        ]);
    }
}
