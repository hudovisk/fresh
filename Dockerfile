FROM  php:7.1-fpm

LABEL maintainer="hudo.assenco@gmail.com"

# netcat - To wait for mysql connection
RUN apt-get update && \
    apt-get install -y \
      git \
      unzip \
      netcat

# MySQL Drivers
RUN docker-php-ext-install pdo pdo_mysql

#XDebug to test-coverage
RUN pecl install xdebug-2.5.0 \
    && docker-php-ext-enable xdebug

# Allow Composer to be run as root
ENV COMPOSER_ALLOW_SUPERUSER 1

# Install Composer and make it available in the PATH
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin/ --filename=composer

# PROJECT
ADD . /opt/apps/laravel

WORKDIR /opt/apps/laravel

# Run composer install
RUN composer install

RUN chmod a+x ./.docker/*.sh

CMD ["/bin/bash", "-c", "chmod a+x ./.docker/laravel-production.sh && ./.docker/laravel-production.sh"]
