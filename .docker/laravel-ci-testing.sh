#!/bin/bash

export APP_ENV=testing

#Waiting for mysql
until nc -z -v -w30 mysql 3306
do
  echo "Waiting for database connection..."
  # wait for 5 seconds before check again
  sleep 5
done

#Run Migrations
php artisan migrate --seed

./.docker/initial-setup.sh

#Run the tests
./vendor/bin/phpunit --coverage-text --colors=never